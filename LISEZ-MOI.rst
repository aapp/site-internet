Site internet de l'Association Au Plus Près
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le site est basé sur deux projets:

* `Start Bootstrap - Creative <https://startbootstrap.com/themes/creative/>`__

  Version 5.1.8

  Commit ``5561394ffff96265376550e80d317b603425ae8b`` du
  `déptôt git sur GitHub <https://github.com/BlackrockDigital/startbootstrap-creative>`__

* `Start Bootstrap - Freelancer <https://startbootstrap.com/themes/freelancer/>`__

  Version 5.1.3

  Commit ``70d1c8b354dcc8d8eb3a9431b60765d51cc4d7f9`` du
  `déptôt git sur GitHub <https://github.com/BlackrockDigital/startbootstrap-freelancer>`__

Compilation
-----------

Pour compiler le projet:

* Installer les dépendances en lançant la commande ``npm install``

* Compilez le projet avec ``gulp``

* Les fichiers à mettre en ligne sont ``index.html``, ``css/aapp.min.css``,
  ``js/aapp.min.js``, le répertoire ``img`` et le répertoire ``vendor``
